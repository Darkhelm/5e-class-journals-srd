<h1>5e Class Journals SRD</h1>
<p>5e Class Journals SRD is a module for Foundry VTT 5th Edition which adds two compendiums of jounral entries for the 5e system.
The compendiums contain Journal entries for class descriptions including feature and spell links which can transferred 
via drag and drop to PC character sheets. </p>

Included Compendiums:<br>
<ul>
<li>Class Journals SRD contains a journal for each class describing the class features</li>
<li>Class Spell Lists SRD which contains a journal entry for each class with links to 
their spells by level.</li>
</ul>

Game content is distributed
under the Open Gaming License v1.0a.
The Systems Reference Document (SRD) for included
content is available in full from Wizards of the Coast.

Installation Instructions
To install the Class Journal for Foundry Virtual Tabletop, simply paste the following URL into the Manifest URL:
dialog on the Install Modules menu of the application.
https://gitlab.com/Darkhelm/5e-class-journals-srd/-/blob/master/module.json



